import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    query = {"query": f"{city}, {state}", "per_page": 1}
    # Create the URL for the request with the city and state
    # Make the request
    response = requests.get(url, params=query, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    # {
    #     "weather": {
    #         "temp": temperature in Fahrenheit (imperial measure),
    #         "description": the description of the weather,
    # like "overcast clouds"
    #     }
    # Make the request
    url = "http://api.openweathermap.org/geo/1.0/direct"
    query = {
        "q": f"{city}, {state}",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }  # noqa
    response = requests.get(url, params=query)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        latitute = content[0]["lat"]
        longitute = content[0]["lon"]
    except (KeyError, IndexError):
        return {
            "weather": "It looks like you dont have a valid city and state",
            "city": city,
            "state": state,
        }  # noqa
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    url = "https://api.openweathermap.org/data/2.5/weather"
    query = {
        "lat": latitute,
        "lon": longitute,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }  # noqa
    response = requests.get(url, params=query)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    print(content)
    try:
        temp = content["main"]["temp"]
        description = content["weather"][0]["description"]
        return {"weather": {"temperature": temp, "description": description}}
    except (KeyError, IndexError):
        return {"weather": None, "city": city, "state": state}
